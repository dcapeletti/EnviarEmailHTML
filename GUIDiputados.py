# coding= utf-8

'''
Created on 25/03/2017

@author: diegoariel
Prueba
'''

import wx
import GUIConfigSMTP

class GUIDiputados(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, None, wx.ID_ANY, 'Polimail')
        self.configurarGUI()
        
    def configurarGUI(self):
        self.barra_herramientas = self.CreateToolBar() #Creo la barra de herramientas
        self.configSMTPServer = self.barra_herramientas.AddLabelTool(wx.ID_ANY, u'Configurar correo electrónico', wx.Bitmap("icons/SMTP-server.ico", wx.BITMAP_TYPE_ICO))
        self.filtro_emails = self.barra_herramientas.AddLabelTool(wx.ID_ANY, u"Filtrar emails", wx.Bitmap("icons/Filtro.ico", wx.BITMAP_TYPE_ICO))
        
        self.barra_herramientas.Realize()
        
        self.Bind(wx.EVT_TOOL, self.ConfigSMTPServer, self.configSMTPServer)
        
    def ConfigSMTPServer(self, event):
        #Configuración del servidor SMTP, debería ser en panel.
        confif_smtp = GUIConfigSMTP.ConfigNotificaciones(self)
        #confif_smtp.ShowModal()
        #confif_smtp.CenterOnScreen()
        #confif_smtp.Destroy()
        
    
    
app = wx.App()
f = GUIDiputados(None)
f.Show()
f.CenterOnScreen()
app.MainLoop()
    