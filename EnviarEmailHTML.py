# coding=utf-8

"""
    Copyright © 2014-2015 Diego Ariel Capeletti

    This file is part of EnviarEmailHTML, a Qt-based graphical interface for Tox.

    qTox is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qTox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qTox.  If not, see <http://www.gnu.org/licenses/>.
"""

import smtplib, time

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# remitente == Correo desde el que voy a enviar
# destinatario == Direcciones a las que quiero enviar separadas por coma, sin espacios.
print "Iniciando sesión de email..."
remitente = "d.a.capeletti@openmailbox.org"
destinatario = "d.a.capeletti@openmailbox.org, d.a.capeletti@gmail.com"

lista_emails = destinatario.split(",") #si existe un solo email, no hay comas y esta variable solo contiene dicho email.

mail = smtplib.SMTP('smtp.openmailbox.org', 587)

mail.ehlo()

mail.starttls()
try:
    mail.login('d.a.capeletti@openmailbox.org', 'xxx') #xxx debe reemplazarlo por su contraseña.
except smtplib.SMTPAuthenticationError:
    print "Usuario o contraseña incorrectos."

for x in range(0,len(lista_emails)): #recorro todas la lista de emails en destinatario y envio el mensaje a cada email.
    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Link" #Asunto del mensaje
    msg['From'] = remitente
    email = lista_emails[x].strip() #quito cualquier espacio que haya, por ejemplo al separarlo con coma en destinatario
    msg['To'] = email #Cco, con copia oculta
    
    # Create the body of the message (a plain-text and an HTML version).
    text = "Hi!\nHow are destinatario?\nHere is the link destinatario wanted:\nhttp://www.python.org"
    html = "<head><body>Hola, como estás? <strong>bien vos</strong></body></head>"
    
    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html', 'utf-8') #uft-8 para soporte de acentos
    
    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)
    # Send the message via local SMTP server.

    print x, "de", len(lista_emails), ".Enviando email a", email
    mail.sendmail(remitente, email, msg.as_string())
    time.sleep(3)

mail.quit()
