# coding= ISO-8859-1

'''
Created on 09/09/2016

@author: diegoariel

Crear los paneles y poner en cada objeto como padre.
Crear lo sizer.
Agregar los objetos a los sizer.
Establecer los sizer a los paneles creados en el punto 1.

El sizer padre de todos que se establece al Frame, debe contener a los paneles.
'''

import wx

class ConfigNotificaciones(wx.Panel):
    def __init__(self, parent):
        #style = wx.DEFAULT_FRAME_STYLE & (~wx.MAXIMIZE_BOX) & (~wx.RESIZE_BORDER)
        size=(470, 250)
        #wx.Frame.__init__(self, None, wx.ID_ANY, u'Configuración de alertas', style=style)
        wx.Panel.__init__ ( self, parent)
        self.tamanoCuadroTexto=25
        self.crearGUI()
    
    def crearGUI(self):
        panelConfServer = wx.Panel(self, wx.ID_ANY)
        cajaAgrupador = wx.StaticBox(panelConfServer, -1, u'Configuración del servidor')
        #panelConfServer.SetBackgroundColour('green')
        labelServerSMTP = wx.StaticText(panelConfServer, -1, 'Servidor SMTP:')
        txtServidorSMTP = wx.TextCtrl(panelConfServer, -1, size=(150, self.tamanoCuadroTexto))
        labelPuerto = wx.StaticText(panelConfServer, -1, 'Puerto:')
        txtPuerto = wx.TextCtrl(panelConfServer, -1, size=(50, self.tamanoCuadroTexto))
        lblUsuario = wx.StaticText(panelConfServer, -1, 'Usuario:')
        txtUsuario = wx.TextCtrl(panelConfServer, -1, size=(150, self.tamanoCuadroTexto))
        lblContrasena = wx.StaticText(panelConfServer, -1, u'Contraseña:')
        txtContrasena = wx.TextCtrl(panelConfServer, -1, size=(150, self.tamanoCuadroTexto), style=wx.TE_PASSWORD)     
        sizer = wx.FlexGridSizer(2, 4)
        #sizer.Add(cajaAgrupador)
        sizer.Add(labelServerSMTP, 0, wx.ALL, 2)
        sizer.Add(txtServidorSMTP, 0, wx.ALL, 2)
        sizer.Add(labelPuerto, 0, wx.ALL, 2)
        sizer.Add(txtPuerto, 0, wx.ALL, 2)
        sizer.Add(lblUsuario, 0, wx.ALL, 2)
        sizer.Add(txtUsuario, 0, wx.ALL, 2)
        sizer.Add(lblContrasena, 0, wx.ALL, 2)
        sizer.Add(txtContrasena, 0, wx.ALL, 2)
        sizer.Add(wx.Button(panelConfServer, -1, u"Probar configuración"), 0, wx.ALL, 2)
        sizerCajaAgrupadora = wx.StaticBoxSizer(cajaAgrupador)
        sizerCajaAgrupadora.Add(sizer, 0, wx.EXPAND | wx.ALL, 5)
        panelConfServer.SetSizer(sizerCajaAgrupadora)
               
        panelBotonera = wx.Panel(self, -1)
        btnAceptar=wx.Button(panelBotonera, wx.ID_OK)
        btnCancelar=wx.Button(panelBotonera, wx.ID_CANCEL)
        sizerBotonera = wx.BoxSizer(wx.HORIZONTAL)
        sizerBotonera.Add(btnAceptar)
        sizerBotonera.Add(btnCancelar)
        panelBotonera.SetSizer(sizerBotonera)
                
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(panelConfServer, 1, wx.EXPAND | wx.ALL, 5) #Espacio libre en los bordes
        mainSizer.Add(panelBotonera, 0, wx.ALIGN_RIGHT, 0) #Los alineo a la derecha
        self.SetSizer(mainSizer)
        mainSizer.Fit(self) #ajusto al tamano minimo
        self.SetBackgroundColour(wx.NullColour) #color por defecto del sistema
        self.Layout()
        self.Center()
        
        
'''
app = wx.App()
frame = ConfigNotificaciones(None)
frame.Show()
app.MainLoop()
'''













