Script enviar email python

Este script pretende ser una herramienta para enviar emails en formato HTML a una lista de usuarios en forma de copia oculta CCO.
Si el servidor no soporta varias casillas CCO, el cript debe usar un bucle para enviar a cada casilla de la lista de emails. De esa manera, podríamos usar cualquier servidor de origen.
Muchas veces queremos comunicarnos con Diputados y Cenadores de nuestro país, presentar una idea o reclamo. Porque no tener una herramienta para automatizar este procedimiento.